(function(){
  jQuery(window).scroll(function () {
      var top = jQuery(document).scrollTop();
      jQuery('.splash').css({
        'background-position': '0px -'+(top/3).toFixed(2)+'px'
      });
      if(top > 50)
        jQuery('#home > .navbar').removeClass('navbar-transparent');
      else
        jQuery('#home > .navbar').addClass('navbar-transparent');
  });

  jQuery("a[href='#']").click(function(e) {
    e.preventDefault();
  });

  var jQuerybutton = jQuery("<div id='source-button' class='btn btn-primary btn-xs'>&lt; &gt;</div>").click(function(){
    var html = jQuery(this).parent().html();
    html = cleanSource(html);
    jQuery("#source-modal pre").text(html);
    jQuery("#source-modal").modal();
  });

  jQuery('.bs-component [data-toggle="popover"]').popover();
  jQuery('.bs-component [data-toggle="tooltip"]').tooltip();

  jQuery(".bs-component").hover(function(){
    jQuery(this).append(jQuerybutton);
    jQuerybutton.show();
  }, function(){
    jQuerybutton.hide();
  });

  function cleanSource(html) {
    var lines = html.split(/\n/);

    lines.shift();
    lines.splice(-1, 1);

    var indentSize = lines[0].length - lines[0].trim().length,
        re = new RegExp(" {" + indentSize + "}");

    lines = lines.map(function(line){
      if (line.match(re)) {
        line = line.substring(indentSize);
      }

      return line;
    });

    lines = lines.join("\n");

    return lines;
  }

})();
