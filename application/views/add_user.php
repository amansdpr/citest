<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css" >
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.min.css" >
	<link rel="stylesheet" href="<?php echo base_url();?>assets/style.css" >
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<?php echo form_open_multipart('home/add_user', array('class' => 'form-horizontal')); ?>
					<legend>Add new user</legend>
					<div class="form-group">
				        <label for="name" class="col-lg-2 control-label">Email</label>
				        <div class="col-lg-10">
				          	<input type="text" name="name" class="form-control" id="name" placeholder="Email">
				        </div>
				  	</div>

				    <input type="submit" name="submit" value="Create new user" />

				</form>
			</div>
		</div>
	</div>








	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js" ></script>

</body>
</html>