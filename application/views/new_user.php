<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css" >
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.min.css" >
	<link rel="stylesheet" href="<?php echo base_url();?>assets/style.css" >
</head>
<body>
	<h2>Add new user</h2>

	<?php //echo form_open('home/add_user'); ?>

	    <label for="name">Name</label>
	    <input type="text" name="name" /><br />

	    <label for="email">Email</label>
	    <textarea name="email"></textarea><br />

	    <input type="submit" name="submit" value="Create new user" />

	</form>

<h1><?php echo base_url();?>js/jquery-2.1.4.min.js</h1>

<ul class="pagination pagination-sm">
                <li class="disabled"><a href="#">«</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">»</a></li>
              </ul>


	<script type="text/javascript" src="<?php echo base_url();?>js/jquery-2.1.4.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/custom.js" ></script>

</body>
</html>