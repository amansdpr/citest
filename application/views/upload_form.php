<html>
<head>
<title>Upload Form</title>
</head>
<body>
 
 
<?php echo form_open_multipart('upload/do_upload');?>

	<label for="inputFile">
		<img id="image_upload_preview" src="http://placehold.it/200x150" alt="your image" width="200" />
	</label>
	<input type='file' id="inputFile" name="userfile"  hidden />

<!-- 	<br><br>
	<input type="text" name="name" placeholder="Name" value="<?php echo set_value('name')?>" >
	<br><br>
	<input type="text" name="email" placeholder="Email" value="<?php echo set_value('email')?>" >
	<br><br> -->
	 
	<input type="submit" value="upload" name="upload" />

	<br>

	<?php echo validation_errors(); ?>
 
</form>


<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js" ></script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputFile").change(function () {
        readURL(this);
    });
</script>
 
</body>
</html>