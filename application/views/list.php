<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css" >
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.min.css" >
	<link rel="stylesheet" href="<?php echo base_url();?>assets/style.css" >
</head>
<body>
<?php
// echo "<pre>";
// print_r($email_lists);
// echo "</pre>";

// echo count($email_lists);

?>

	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<legend>User email lists</legend>
				<ul>
					<?php foreach ($email_lists as $value) { ?>
						<li><?php echo $value['email'] ?></li>
					<?php } ?>
				</ul>

				<?php echo $pagination; ?>
				<br>
				<a href="<?php echo base_url();?>home/add_user">Add New User</a>
			</div>
		</div>
	</div>				


	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js" ></script>

</body>
</html>