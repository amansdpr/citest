<?php


class Email_list_model extends CI_Model {

	public function get_email_lists($data){

		try {
			$this->db->select('email');
	        $query = $this->db->get('user', $data['offset'], $data['limit']);
	        $query_result = $query->result_array();

	        if (count($query_result)) {
	            return $query_result;
	        }
        } catch (Exception $e) {
            return false;
        }
	}


	public function email_total()
	{
		try {
			$query_result = $this->db->get('user')->num_rows();

			if (count($query_result)) {
				return $query_result;
			}
        } catch (Exception $e) {
            return false;
        }
	}

}