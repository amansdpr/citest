<?php


class Home extends CI_Controller {

	public function index(){

		$this->load->model('email_list_model');
		$this->load->library('pagination');

		$config['base_url'] = 'http://localhost/citest/home/index';
		$config['total_rows'] = $this->email_list_model->email_total();
		$config['per_page'] = 10;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$this->pagination->initialize($config); 
		




		$data = array(
            'limit'         => $this->uri->segment(3),
            'offset'        => $config['per_page']
        );

		$data['email_lists'] = $this->email_list_model->get_email_lists($data);
		$data['pagination'] = $this->pagination->create_links();






		// echo "<pre>";
		// var_dump($data['total_rows']);
		// echo "</pre>";

		$this->load->view('list', $data);
	}



	public function add_user()
	{
		$this->load->view('add_user');
	}


}